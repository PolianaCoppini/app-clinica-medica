import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    title: {
        fontSize: 24,
        color: '#FF000',
        textAlign: 'center'
    },
    subtitle: {
        fontSize:17,
        color: '#999999',
        textAlign: 'center'
    }
});

export default StyleSheet.create({

});