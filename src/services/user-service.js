import {Store} from '../store';
import {UserAction as Action} from '../actions';

export const reducerKey = 'user';//para fazer referencia e buscar o user dentro do reducer
export const login = payload => {
    Store.dispatch({
        type: Action.SET,
        payload,
    });
};

export const logout = () => {
    Store.dispatch({
        type: Action.LOGOUT,
    });
};