import React from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image, ImageBackground } from 'react-native';

// Componente de tela de login
const TelaLogin = () => {
  return (
    <ImageBackground style={styles.container}>
      <View style={styles.overlay}>
      <Image source={require('../../imagens/login/inon_login.png')} style={styles.logo} />

      <Text style={styles.criarConta}>Doutora Online</Text>

      <TextInput style={styles.input} placeholder="Email" />
      <TextInput style={styles.input} placeholder="Senha" secureTextEntry={true} />
      <TouchableOpacity style={styles.botaoCadastrar}>
        <Text style={styles.textoBotao}>Entrar</Text>
      </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

// Estilos
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  overlay: {
    backgroundColor: 'rgba(255, 182, 193, 0.8)',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },
  criarConta: {
    fontSize:35,
    fontWeight: 'bold',
    color: 'white',
    marginBottom: 20,
  },
  input: {
    width: '80%',
    height: 40,
    backgroundColor: 'white',
    marginBottom: 10,
    paddingLeft: 10,
    borderRadius: 5,
  },
  botaoCadastrar: {
    backgroundColor: 'pink',
    width: '80%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: 10,
  },
  textoBotao: {
    color: 'white',
    fontWeight: 'bold',
    fontSize:30
  },
});

export default TelaLogin;