// // src/navigation/StackNavigator.js
// import React from 'react';
// import { NavigationContainer } from '@react-navigation/native';
// import { createStackNavigator } from '@react-navigation/stack';

// import TelaLogin from '../screens/Autenticacao/TelaLogin';
// import TelaHome from '../screens/TelaHome/TelaHome';

// const Stack = createStackNavigator();

// const StackNavigator = () => {
//   return (
//     <NavigationContainer>
//       <Stack.Navigator initialRouteName="TelaLogin">
//         <Stack.Screen name="TelaLogin" component={TelaLogin} />
//         <Stack.Screen name="TelaHome" component={TelaHome} />
//       </Stack.Navigator>
//     </NavigationContainer>
//   );
// };

// export default StackNavigator;