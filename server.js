// const express = require('express');
// const { Pool } = require('pg');
// const bodyParser = require('body-parser');
// const cors = require('cors');
// const bcrypt = require('bcrypt');

// const app = express();
// const PORT = 3001;

// app.use(cors());
// app.use(bodyParser.json());

// const pool = new Pool({
//   user: 'usuarioAindaNaoDefinido',
//   host: 'localhost',
//   database: 'sua_database',
//   password: 'sua_senha',
//   port: 5432,
// });

// // Rota para autenticação
// app.post('/login', async (req, res) => {
//   const { email, senha } = req.body;

//   try {
//     const result = await pool.query('SELECT * FROM usuarios WHERE email = $1', [email]);

//     if (result.rows.length === 0) {
//       return res.status(401).json({ error: 'Credenciais inválidas' });
//     }

//     const usuario = result.rows[0];
//     const senhaCorreta = await bcrypt.compare(senha, usuario.senha);

//     if (senhaCorreta) {
//       res.json({ mensagem: 'Autenticação bem-sucedida' });
//     } else {
//       res.status(401).json({ error: 'Credenciais inválidas' });
//     }
//   } catch (error) {
//     console.error('Erro de autenticação:', error.message);
//     res.status(500).json({ error: 'Erro interno do servidor' });
//   }
// });

// app.listen(PORT, () => {
//   console.log(`Server is running on http://localhost:${PORT}`);
// });