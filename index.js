import { AppRegistry } from 'react-native';
import StackNavigator from './src/navigation/StackNavigator'; 
import { name as appName } from './app.json';

const App = () => {
  return <StackNavigator />;
};

AppRegistry.registerComponent(appName, () => App);